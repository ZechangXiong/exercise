# 前言

这是一个菜鸡自学完vue项目之后

**注：本项目纯属个人瞎搞，十分不成熟，优化空间巨大**

## 技术栈

vue2 + vue-cli + vuex + vue-router + webpack + ES6 + elementUI

## 项目运行

```
git clone git@gitee.com:hell0-w0rld/exercise.git

npm install

npm run dev

```

## 关于接口数据

此项目的所有接口来源于配套的后台系统。后台项目传送地址：（待填）

## 目标功能

- [x] 用户登录
- [x] 验证码
- [x] 注册
- [x] 用户名密码格式验证
- [x] 密码加密
- [x] 用户基本信息展示和修改
- [x] 密码修改
- [ ] 上次登录时间
- [x] 用户列表：分页、模糊搜索、姓名排序
- [x] 较友好的提示信息


## 说明

如果对您有帮助，您可以点右上角“star”支持一下

**特别感谢丰哥！！！fgnb,辛苦了🌹**

## 效果演示

[查看demo](http://106.13.105.163:8080/dist/#/login)

## 技术总结

[查看技术总结](https://blog.csdn.net/qq_46747888/article/details/116425801)

