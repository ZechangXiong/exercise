import Vue from "vue"
import Vuex from "vuex"
Vue.use(Vuex);
export default new Vuex.Store({
    state:{
      // 自定义的全局变量
        token:"",
        user_name:"",
        user_password:"",
        user_phone:"",
        user_email:""
    },
    getters:{
        getToken(state){
           return state.token;
        },
        getName(state){
            return state.user_name;
        },
        getPassword(state){
          return state.user_password;
        },
        getPhone(state){
          return state.user_phone;
        },
        getEmail(state){
          return state.user_email;
        }

    },
    mutations:{
      // 方法
        // 保存当前菜单栏的路径
        setToken(state,currToken){
            state.token = currToken;
        },
        // 保存当前点击的数据源
        setPassword(state,currUserPassword){
            state.user_password = currUserPassword;
        },
        // 保存当前点击的元数据
        setName(state,currUserName){
            state.user_name = currUserName;
        },
        // 保存所有数据源
        setEmail(state,currEmailNumber){
            state.user_email = currEmailNumber;
        },
        setPhone(state,currPhoneNumber){
            state.user_phone = currPhoneNumber;
        },
        setInit(state){
          state.token = "";
          state.user_name = "";
          state.user_password = "";
          state.user_phone = "";
          state.user_email = "";
        }
      }
})