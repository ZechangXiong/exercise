// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import axios from 'axios'
import Elementui from 'element-ui'
import App from './App'
import store from './store/store'
import router from '../router'

import 'element-ui/lib/theme-chalk/index.css'
import '../config/axios'
import '../static/style.css'

Vue.prototype.$http = axios
Vue.config.productionTip = false

Vue.use(Elementui)

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  components: { App },
  template: '<App/>'
})
// 导航守卫
router.beforeEach((to,from,next) => {
  // console.log(to);
  console.log(store.getters.getToken);
  // console.log(next);
  // 如果有token值，放行
  if(store.getters.getToken || to.path === '/'){
    next();
  }else{
    // 如果没有token，则跳转到登陆页面
    next({
      path:'',
    })
  }
  next();
})