
import Vue from 'vue'
import VueRouter from 'vue-router'

//安装路由
Vue.use(VueRouter);
// 配置导出路由
export default new VueRouter({
    routes: [
        {
            path:'',
            redirect:'/login'
        },
        {
            path:'/login',       
            component: r => require.ensure([],() => r(require('../src/components/Login')),'home')
        },
        {
            path:'/detail',
            component: r => require.ensure([],() => r(require('../src/components/Detail')),'home')
        }
    ]
});